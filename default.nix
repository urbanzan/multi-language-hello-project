{ pkgs ? import <nixpkgs> { } }:

{
  hello_b = pkgs.callPackage ./src/hello_b.nix { inherit pkgs; };
  hello_c = pkgs.callPackage ./src/hello_c.nix { inherit pkgs; };
  hello_cpp = pkgs.callPackage ./src/hello_cpp.nix { inherit pkgs; };
  hello_rust = pkgs.callPackage ./src/hello_rust.nix { inherit pkgs; };
}
