{
  description = "Multi language Hello project";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            (final: prev: { hello = import ./default.nix { pkgs = prev; }; })
          ];
        };
      in {
        packages."hello" = pkgs.symlinkJoin {
          name = "hello";
          paths = with pkgs.hello; [ hello_b hello_c hello_cpp hello_rust ];
        };
      });
}
