{ pkgs ? import <nixpkgs> { } }:

pkgs.stdenv.mkDerivation {
  name = "hello_cpp";
  src = ./.;
  nativeBuildInputs = with pkgs; [ boost poco ];
  buildPhase = "c++ -o hello_cpp hello.cpp -lPocoFoundation -lboost_system";
  installPhase = "mkdir -p $out/bin; install -t $out/bin hello_cpp";
}
